using System.Configuration; 
using System.Data.SqlClient;
using ConsoleTableExt;

namespace excelreader
{
    class Database
    {
        private static string cs = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;

        List<DataList> dataList = new List<DataList>();

//Creates excelreader Table with an ID column
        public void CreateTable()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText = "DROP TABLE IF EXISTS excelreader";
                    command.ExecuteNonQuery();
                    command.CommandText =
                        @"IF OBJECT_ID('dbo.excelreader', 'U') IS NULL 
                        CREATE TABLE excelreader(
                        ReaderID INTEGER IDENTITY(1,1) NOT NULL PRIMARY KEY, 
                        )";
                    command.ExecuteNonQuery();
                }
            }
        }        

//Adds all available Columns from ExcelTable
        public void AlterTable(string headerName, string dataType)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText =
                        @"Declare @SQL VarChar(128), @COLUMN VarChar(128), @TYPE VarChar(128)
                        SET @COLUMN = @headername
                        SET @TYPE = @datatype
                        SELECT @SQL = 'ALTER TABLE excelreader ADD ' + @COLUMN + ' ' + @TYPE
                        Exec (@SQL)
                        ";
                    command.Parameters.Add(new SqlParameter("@headerName", headerName));
                    command.Parameters.Add(new SqlParameter("@datatype", dataType));
                    command.ExecuteNonQuery();
                }
            }
        }
    
    //Fills for all rows the first column with a temporary value
        public void TempFillCell(string headerName)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    //string insertValue = "(" + 1 + ")";
                    string insertValue = "(" + 1 + ")";
                    headerName = "(" + headerName + ")";
                    connection.Open();
                    command.CommandText =
                            @"DECLARE @Value varchar(128),@SQL varchar(128), @ColumnName varchar(128)
                            SET  @Value = @insertValue
                            SET @ColumnName = @headerName

                            SET @SQL = 'INSERT INTO excelreader' + ' ' + @ColumnName + ' ' +
                            'VALUES' + ' '  + @Value
                            EXEC (@SQL)
                            ";
                    command.Parameters.Add(new SqlParameter("@insertValue", insertValue));
                    command.Parameters.Add(new SqlParameter("@headerName", headerName));
                    command.ExecuteNonQuery();
                }
            }
        }

//Fill Excel Strings into DB
        public void FillCell(string cellValue, int id, string currentHeader)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    cellValue = "'" + cellValue + "'";
                    connection.Open();
                    command.CommandText =
                            @"declare @SQL varchar (128), @VALUE varchar(128), @TABLE varchar(128), @RID varchar(128)
                            SET @VALUE = @cellValue
                            SET @TABLE = 'excelreader'
                            SET @RID = 'ReaderID'

                            SET @SQL = 'UPDATE ' + @TABLE + ' SET ' + @currentHeader + '=' + @VALUE +
                                           ' WHERE ' + @RID + '=' + @id;
                            EXEC (@SQL)
                            ";
                    command.Parameters.Add(new SqlParameter("@currentHeader", currentHeader));
                    command.Parameters.Add(new SqlParameter("@id", id.ToString()));
                    command.Parameters.Add(new SqlParameter("@cellValue", cellValue));
                    command.ExecuteNonQuery();
                }
            }
        }

//Fill Excel Numbers into DB
        public void FillCell(double cellValue, int id, string currentHeader)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText =
                            @"declare @SQL varchar (128), @VALUE varchar(128), @TABLE varchar(128), @RID varchar(128)
                            SET @VALUE = @cellValue
                            SET @TABLE = 'excelreader'
                            SET @RID = 'ReaderID'

                            SET @SQL = 'UPDATE ' + @TABLE + ' SET ' + @currentHeader + '=' + @VALUE +
                                           ' WHERE ' + @RID + '=' + @id;
                            EXEC (@SQL)
                            ";
                    command.Parameters.Add(new SqlParameter("@currentHeader", currentHeader));
                    command.Parameters.Add(new SqlParameter("@id", id.ToString()));
                    command.Parameters.Add(new SqlParameter("@cellValue", cellValue));
                    command.ExecuteNonQuery();
                }
            }
        }

//Fill Excel Dates into DB
        public void FillCell(DateTime cellValue, int id, string currentHeader)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText =
                            @"UPDATE excelreader 
                            SET " + @currentHeader + "= @cellValue WHERE ReaderID = @id";
                            //@"declare @SQL varchar (128), @COLUMN varchar(128)
                            //SET @COLUMN = 'DATE' 

                            //SET @SQL = 'UPDATE excelreader 
                            //            SET ' + @COLUMN + '=' + @cellValue
                            //            + 'WHERE ReaderID' + '=' + @id;
                            //EXEC (@SQL)
                            //";
                    command.Parameters.Add(new SqlParameter("@currentHeader", currentHeader));
                    command.Parameters.Add(new SqlParameter("@id", id.ToString()));
                    command.Parameters.Add(new SqlParameter("@cellValue", cellValue));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void ShowExcelDB()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText = @"SELECT count(*) FROM excelreader";
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        string output = 
                                @"SELECT * FROM excelreader 
                                ORDER BY ReaderID ASC
                                ";
                        SqlCommand command1 = new SqlCommand(output, connection);
                        SqlDataReader reader = command1.ExecuteReader();
                        while (reader.Read() == true)
                        {
                            dataList.Add(new DataList() {Word = reader[1].ToString(), 
                            Number = Convert.ToDouble(reader[2]), 
                            Date = DateOnly.FromDateTime(Convert.ToDateTime(reader[3]))
                            });
                        }
                    }
                    else
                    {
                        Console.WriteLine("There are no entries yet");
                    }
                }
                ConsoleTableBuilder
                .From(this.dataList)
                .ExportAndWrite();
                this.dataList.Clear();
                Console.WriteLine();
            }

        }

    }
}