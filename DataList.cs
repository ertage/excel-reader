using System;

namespace excelreader
{
    public class DataList
    {
        public string Word {get; set;}
        public double Number {get; set;}
        public DateOnly Date {get; set;}
    }

}
