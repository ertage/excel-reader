using OfficeOpenXml;
using System.Globalization;

namespace excelreader
{
    class ExcelAddToDB
    {

        FileInfo fi = new FileInfo(@"testsheet.xlsx");
        Database db = new Database();
        private CultureInfo cultureInfo = new CultureInfo("de-DE");
        public void CreateExcelTable()
        {
            db.CreateTable();
        }
        public void ExcelCreateHeaders()
        {
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                int numCol = firstWorksheet.Dimension.Columns;

                for(int x=1; x<=numCol; x++)
                {
                    string excelHeader = firstWorksheet.Cells[1,x].Value.ToString();
                    var dataType = firstWorksheet.Cells[2,x].Value;
                    string tempString = dataType.ToString();

                    switch(Type.GetTypeCode(dataType.GetType()))
                        {
                            case TypeCode.String:
                            if (DateTime.TryParseExact(tempString,"dd.MM.yyyy", cultureInfo, DateTimeStyles.None, out DateTime Temp) == true)
                            {
                                tempString = "DateTime";
                                db.AlterTable(excelHeader, tempString);
                            }
                            else
                            {
                                tempString = "VarChar(128)";
                                db.AlterTable(excelHeader, tempString);
                            }
                            break;
                            case TypeCode.Double:
                                tempString = "INT";
                                db.AlterTable(excelHeader, tempString);
                            break;
                            default:
                                tempString = "text";
                                db.AlterTable(excelHeader, tempString);
                            break;
                        }
                }
                excelPackage.Save();
            }
            
        }

        public void FillCellDB()
        {
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                string tempString = "";
                int id = 1;
                int numCol = firstWorksheet.Dimension.Columns;
                int numRow = firstWorksheet.Dimension.Rows;
                string[] formats= {"MM/dd/yyyy hh:mm:ss tt", "yyyy-MM-dd hh:mm:ss", "dd.MM.yyyy", "ddMMyy"};

                //initializing rows with a temporary value
                for(int y=2; y<=numRow;y++)
                {
                    var excelCell = firstWorksheet.Cells[y,1].Value;
                    string headerName = firstWorksheet.Cells[1,1].Value.ToString();
                    db.TempFillCell(headerName.ToString());
                }

                //Filling Cells with actual values
                for(int x=1; x<=numCol; x++)
                {
                    for(int y=2; y<=numRow; y++)
                    {
                        var excelCell = firstWorksheet.Cells[y,x].Value;
                        string currentHeader = firstWorksheet.Cells[1,x].Value.ToString();

                        if(excelCell == null)
                        {
                        }
                        else
                        {
                        switch(Type.GetTypeCode(excelCell.GetType()))
                        {
                            case TypeCode.String:
                            tempString = excelCell.ToString();
                            if (DateTime.TryParseExact(tempString,formats, cultureInfo, DateTimeStyles.None, out DateTime dateValue) == true)
                            {
                            db.FillCell(dateValue, id, currentHeader);
                            }
                            else
                            {
                            db.FillCell(excelCell.ToString(),id, currentHeader);
                            }
                            break;
                            case TypeCode.Double:
                            db.FillCell(Convert.ToDouble(excelCell), id, currentHeader);
                            break;
                            default:
                            //unspecified datatype
                            db.FillCell(excelCell.ToString(), id, currentHeader);
                            break;
                        }
                        id++;
                        }
                    }
                    id = 1;

                }
                excelPackage.Save();
            }
        }
        public void ExcelShow()
        {
            db.ShowExcelDB();
        }


    }

}
