﻿using System;
using OfficeOpenXml;

namespace excelreader
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelAddToDB edb = new ExcelAddToDB();
            Console.WriteLine("Creating Table....");
            edb.CreateExcelTable();
            Console.WriteLine("Reading Data...");
            edb.ExcelCreateHeaders();
            edb.FillCellDB();
            edb.ExcelShow();

        }
    }
}